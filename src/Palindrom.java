import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        // variabel kata
        String kata;
        //Scanner untuk inputan keyboard
        Scanner scan = new Scanner(System.in);
        System.out.println("+------------------------------------------------+");
        System.out.println("|             Palindrome Checker App             |");
        System.out.println("+------------------------------------------------+");
        System.out.print(" Inputkan Kata/ Kalimat : ");
        kata = scan.nextLine();

        //Convert the string ke huruf kecil semua
        kata = kata.toLowerCase();

        // Passing nilai boolean dari fungsi isPalindrome
        if (isPalindrome(kata)) {
            //if boolean true = palindrome
            System.out.println("+------------------------------------------------+");
            System.out.println(" " + kata + " is Palindrome");
        } else {
            System.out.println("+------------------------------------------------+");
            // if boolean false bukan palindrome
            System.out.println(" " + kata + " is not Palindrome");
        }
    }

    // Method
    // mengembalikan nilai true jika string paliindrome
    static boolean isPalindrome(String kata)
    {
        // Pointers mengarah ke awal dan akhir huruf pada string
        // charAwal adalah huruf dari depan
        // charAkhir adalah huruf dari belakang
        int charAwal = 0, charAkhir = kata.length() - 1;

        // Lakukan perulangan selama ada karakter untuk di bandingkan
        while (charAwal < charAkhir) {

            // Jika ada ketidakcocokan return false
            if (kata.charAt(charAwal) != kata.charAt(charAkhir))
                return false;

            charAwal++; // Increment pointer pertama/depan
            charAkhir--; // decrement pointer terakhir/belakang
        }
        return true;
    }
}
